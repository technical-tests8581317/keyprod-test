# Typescript Test for Keyprod

# How does it work ?

Just run the command :

-   npm install
-   npm run test

# Time spent on it

Time taken : 5-6 hours.
Reason is that the instructions give a first framework of how a vending machine works.
But as a developer we notice we could always go further in the features and in the architecture / testing.

# Architecture

The Architecture is divided into 3 main entities :

-   Coin
-   Machine
-   Product

And One Process Entity that involves those 3 entities :

-   Order

Each Module contains :

-   An interface describing the model and its parameters
-   A service composed of Methods dedicated to the service. In the case of the order, since we use other services, we use a library called "Typedi" for dependency injections
-   A test file planning different kind of scenario and exceptions when calling the methods
-   (Optional) A data file in json

# Testings

We use Jest for testing all the domains of our application.
I tried to elaborate different scenarios like the user does not have enough money for buying an article or he tries to put some coins that are not valid.

# Improvements done

-   An inventory of coins in the machine with nickels, dimes and quarters
-   A function for knowing exactly what coins should be returned during the change. A priority is given to the biggest coin.
-   A machine state for knowing if the status of the order is completed or there is a sold out. It makes things easier when the user checks the display.
-   I had to consider the fact there is a message (THANK YOU) at the end if the order is Complete and then it has to switch to INSERT COIN when the whole process has to reboot again. Therefore, I took the choice to write a resetOrder and a resetMachine function which are called 500ms after the completion of the order then the user can still see the message and the machine could be ready for a next order.

# Improvement to do

-   At the moment, we can calculate a change and the number of coins needed. However, it would need to be more complex since it does not take account of the stock of coins of the machine
-   The "Exact Change Only" Function is oversimplified due to the lack of time. At the moment, I am only requesting if the machine has money or not. It does not take account of the price of the products or any small amount of money available in the machine. This process would also need to take account of the fact it depends on the stock of coins of the machine.
