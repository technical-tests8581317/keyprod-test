export enum CoinType {
	'Nickel' = 0.05,
	'Dime' = 0.1,
	'Quarter' = 0.25
}
