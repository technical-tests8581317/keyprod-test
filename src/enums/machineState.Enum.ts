export enum MachineState {
	'active' = 'Active',
	'orderComplete' = 'Order Complete',
	'soldOut' = 'soldOut'
}
