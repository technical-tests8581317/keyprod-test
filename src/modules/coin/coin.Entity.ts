import { CoinType } from '../../enums';
export interface CoinEntity {
	weight: number;
	size: number;
	type: CoinType | null;
}
