import { CoinType } from '../../enums';
import { CoinServices } from './coin.Services';

describe('Coin Testing Services', () => {
	let coinServices: CoinServices;

	beforeAll(() => {
		coinServices = new CoinServices();
	});

	describe('Service : Test validity of the coin', () => {
		it('Nickel should be a valid coin', () => {
			const testNickel = coinServices.isCoinValid('Nickel');
			expect(testNickel).toBe(true);
		});

		it('Dime should be a valid coin', () => {
			const testDime = coinServices.isCoinValid('Dime');
			expect(testDime).toBe(true);
		});

		it('Quarter should be a valid coin', () => {
			const testQuarter = coinServices.isCoinValid('Quarter');
			expect(testQuarter).toBe(true);
		});

		it('Penny should NOT be a valid coin', () => {
			const testPenny = coinServices.isCoinValid('Penny');
			expect(testPenny).toBe(false);
		});
	});

	describe('Service : Set the type of coin', () => {
		it('It should be a dime', () => {
			coinServices.findCoinType(17.91, 2.268);
			expect(coinServices.getCoinType()).toBe(CoinType.Dime);
		});

		it('It should be a nickel', () => {
			coinServices.findCoinType(21.21, 5.0);
			expect(coinServices.getCoinType()).toBe(CoinType.Nickel);
		});

		it('It should be a quarter', () => {
			coinServices.findCoinType(24.257, 5.67);
			expect(coinServices.getCoinType()).toBe(CoinType.Quarter);
		});

		it('It should be a non valid coin', () => {
			coinServices.findCoinType(22.257, 3.77);
			expect(coinServices.getCoinType()).toBe(null);
		});
	});
});
