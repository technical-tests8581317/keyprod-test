import { Container, Service } from 'typedi';
import { CoinType } from '../../enums';
import { CoinEntity } from './coin.Entity';

@Service()
export class CoinServices {
	private readonly coin: CoinEntity = {
		weight: 0,
		size: 0,
		type: null
	};
	constructor(
		size: CoinEntity['size'] = 0,
		weight: CoinEntity['weight'] = 0
	) {
		this.coin.size = size;
		this.coin.weight = weight;
		this.findCoinType(size, weight);
	}

	//Get the type of the coin
	public getCoinType(): CoinEntity['type'] {
		return this.coin.type;
	}

	//Check the validity of the coin depending on its type
	public isCoinValid(type: string): boolean {
		if (type !== null && Object.values(CoinType).includes(type)) {
			return true;
		} else return false;
	}

	//Get the coin value depending on its weights (in g) and on its size (in mm)
	public findCoinType(
		size: CoinEntity['size'],
		weight: CoinEntity['weight']
	): void {
		switch (true) {
			//Dime
			case size === 17.91 && weight === 2.268: {
				this.coin.type = CoinType.Dime;
				break;
			}
			//Nickel
			case size === 21.21 && weight === 5.0: {
				this.coin.type = CoinType.Nickel;
				break;
			}
			//Quarter
			case size === 24.257 && weight === 5.67: {
				this.coin.type = CoinType.Quarter;
				break;
			}
			//Any other case
			default: {
				this.coin.type = null;
				break;
			}
		}
	}
}
