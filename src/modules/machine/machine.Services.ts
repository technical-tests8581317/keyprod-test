import 'reflect-metadata';
import { Service } from 'typedi';
import { CoinType, MachineState } from '../../enums';
import { MachineEntity } from './machine.Entity';

import { ProductServices } from '../product/product.Services';
import { ProductEntity } from '../product/product.Entity';
@Service()
export class MachineServices {
	private readonly machine: MachineEntity = {
		message: 'INSERT COIN',
		state: MachineState.active,
		products: [
			{
				code: 1,
				name: 'Candy',
				quantity: 1
			},
			{
				code: 2,
				name: 'Chips',
				quantity: 2
			},
			{
				code: 3,
				name: 'Cola',
				quantity: 0
			}
		],
		coins: {
			dimes: 5,
			nickels: 10,
			quarters: 15
		},
		totalAmount: 0
	};
	constructor(private productServices: ProductServices) {
		this.calculateAmount();
		this.checkExactChange();
	}

	//Change Coins Quantity
	public changeCoinsQuantity(coins: MachineEntity['coins']): void {
		Object.assign(this.machine.coins, coins);
		this.calculateAmount();
		this.checkExactChange();
	}
	//Find Product
	private findProduct(product: ProductEntity): number {
		const index = this.machine.products.findIndex(
			(p) => p.name === product.name
		);
		return index;
	}

	//This method is aimed for knowing the number of one type of coin needed to cover an amount of money
	public calculateNumberOfCoinsFromAmount(
		amount: number,
		coinValue: number
	): { moneyLeft: number; numberOfCoins: number } {
		const response = {
			moneyLeft: amount,
			numberOfCoins: 0
		};
		const modulo = amount % coinValue;
		if (modulo === 0) {
			response.numberOfCoins = amount / coinValue;
			response.moneyLeft = 0;
		} else {
			response.numberOfCoins = parseFloat(
				(amount / coinValue).toFixed(0)
			);
			response.moneyLeft = parseFloat(
				(amount - response.numberOfCoins * coinValue).toFixed(2)
			);
		}
		return response;
	}

	//Make Change
	//TODO----THIS ALGO DOES NOT TAKE ACCOUNT OF THE COIN CAPACITIES OF THE MACHINE
	public makeChange(changeAmount: number): MachineEntity['coins'] {
		let moneyLeft = changeAmount;
		const coinsToChange: MachineEntity['coins'] = {
			dimes: 0,
			nickels: 0,
			quarters: 0
		};
		//Quarters - 0.25
		const quartersResponse = this.calculateNumberOfCoinsFromAmount(
			moneyLeft,
			0.25
		);
		moneyLeft = quartersResponse.moneyLeft;
		coinsToChange.quarters = quartersResponse.numberOfCoins;
		this.machine.coins.quarters -= quartersResponse.numberOfCoins;

		//Nickels - 0.05
		if (moneyLeft > 0) {
			const nickelsResponse = this.calculateNumberOfCoinsFromAmount(
				moneyLeft,
				0.05
			);
			moneyLeft = nickelsResponse.moneyLeft;
			coinsToChange.nickels = nickelsResponse.numberOfCoins;
			this.machine.coins.nickels -= nickelsResponse.numberOfCoins;
		}

		//Dimes - 0.01
		if (moneyLeft > 0) {
			const dimesResponse = this.calculateNumberOfCoinsFromAmount(
				moneyLeft,
				0.01
			);
			moneyLeft = dimesResponse.moneyLeft;
			coinsToChange.dimes = dimesResponse.numberOfCoins;
			this.machine.coins.dimes -= dimesResponse.numberOfCoins;
		}

		return coinsToChange;
	}

	//Check Exact Change
	public checkExactChange(): void {
		if (this.machine.totalAmount === 0) {
			this.updateMessage('EXACT CHANGE ONLY');
		}
	}

	//Change Machine State
	public changeMachineState(state: MachineState): void {
		this.machine.state = state;
	}

	//Get Total Amount in the machine
	public getMachineTotalAmount(): number {
		return this.machine.totalAmount;
	}

	//Get Machine State
	public getMachineState(): MachineState {
		return this.machine.state;
	}

	//Check Product Avaibility
	public checkProductAvaibility(product: ProductEntity): boolean {
		const index = this.findProduct(product);
		if (this.machine.products[index].quantity > 0) return true;
		else return false;
	}

	//Remove Product
	public decreaseProductQuantity(product: ProductEntity): void {
		const index = this.findProduct(product);
		if (index >= 0 && this.machine.products[index].quantity > 0) {
			this.machine.products[index].quantity--;
		}
	}

	//Update the vending machine message
	public updateMessage(newMessage: string): void {
		this.machine.message = newMessage;
	}

	//Get the vending machine message
	public getMessage(): string {
		return this.machine.message;
	}

	//Reset Machine
	public resetMachine(): void {
		this.machine.message = 'INSERT COIN';
		this.machine.state = MachineState.active;
		this.calculateAmount();
	}

	//Set the total amount in the machine depending on the number of coin
	public calculateAmount(): void {
		const totalAmountDimes = this.machine.coins.dimes * CoinType.Dime;
		const totalAmountNickels = this.machine.coins.nickels * CoinType.Nickel;
		const totalAmountQuarters =
			this.machine.coins.quarters * CoinType.Quarter;

		this.machine.totalAmount =
			totalAmountDimes + totalAmountNickels + totalAmountQuarters;
	}

	//Add to total Amount
	public addToTotalAmount(amount: number): number {
		return this.machine.totalAmount + amount;
	}

	//Get the total Amount
	public getTotalAmount(): number {
		return this.machine.totalAmount;
	}
}
