import { MachineState } from '../../enums';

interface MachineCoinsEntity {
	dimes: number;
	nickels: number;
	quarters: number;
}

interface MachineProductEntity {
	code: number;
	name: string;
	quantity: number;
}

export interface MachineEntity {
	message: string;
	state: MachineState;
	coins: MachineCoinsEntity;
	totalAmount: number;
	products: MachineProductEntity[];
}
