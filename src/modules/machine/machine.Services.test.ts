import { Container } from 'typedi';
import { MachineServices } from './machine.Services';

describe('Machine Testing Services', () => {
	let machineServices: MachineServices;

	beforeAll(() => {
		machineServices = Container.get(MachineServices);
	});

	it('Machine should return the message INSERT COIN', () => {
		const testMessage = machineServices.getMessage();
		expect(testMessage).toBe('INSERT COIN');
	});

	it('Machine should be able to return the total amount', () => {
		expect(machineServices.getTotalAmount()).toBeTruthy();
	});

	it('Machine should be able to calculate the number of coins needed in the change process', () => {
		const response = machineServices.makeChange(1.37);
		expect(response).toMatchObject({
			dimes: 2,
			nickels: 2,
			quarters: 5
		});
	});

	it('Machine should show EXACT CHANGE ONLY if there are no coins available', () => {
		machineServices.changeCoinsQuantity({
			dimes: 0,
			nickels: 0,
			quarters: 0
		});
		expect(machineServices.getMessage()).toBe('EXACT CHANGE ONLY');
	});
});
