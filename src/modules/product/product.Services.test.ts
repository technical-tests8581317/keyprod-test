import { ProductEntity } from './product.Entity';
import { ProductServices } from './product.Services';

describe('Product Testing Services', () => {
	let productServices: ProductServices;

	beforeAll(() => {
		productServices = new ProductServices();
	});

	describe('Service : Set the type of product', () => {
		it('It should be a candy', () => {
			const product_1 = productServices.findProduct(1);
			expect(product_1).not.toBe(null);
			if (product_1) {
				expect(product_1.name).toBe('Candy');
			}
		});

		it('It should be a chips', () => {
			const product_2 = productServices.findProduct(2);
			expect(product_2).not.toBe(null);
			if (product_2) {
				expect(product_2.name).toBe('Chips');
			}
		});

		it('It should be a cola', () => {
			const product_3 = productServices.findProduct(3);
			expect(product_3).not.toBe(null);
			if (product_3) {
				expect(product_3.name).toBe('Cola');
			}
		});

		it('It should be a non valid product', () => {
			const product_null = productServices.findProduct(5);
			expect(product_null).toBe(null);
		});
	});
});
