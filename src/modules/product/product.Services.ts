import { Service } from 'typedi';
import { ProductEntity } from './product.Entity';

import Products from './products.Data.json';

@Service()
export class ProductServices {
	constructor() {}

	//Get Products
	public getProducts(): ProductEntity[] {
		return Products;
	}

	//Choose a product after choosing a code
	public findProduct(code: ProductEntity['code']): ProductEntity | null {
		let foundProduct = Products.find((p) => p.code === code);
		if (foundProduct) {
			return foundProduct;
		} else {
			return null;
		}
	}
}
