export interface ProductEntity {
	code: number;
	name: string;
	price: number;
}
