import 'reflect-metadata';
import { Container, Service } from 'typedi';
import { CoinEntity } from '../coin/coin.Entity';
import { OrderEntity } from './order.Entity';

//Services
import { MachineServices } from '../machine/machine.Services';
import { CoinServices } from '../coin/coin.Services';
import { ProductServices } from '../product/product.Services';
import { ProductEntity } from '../product/product.Entity';
import { MachineState } from '../../enums';

@Service()
export class OrderServices {
	private readonly order: OrderEntity = {
		insertedAmount: 0,
		changeAmount: 0,
		rejectedCoins: 0,
		product: {} as ProductEntity
	};

	constructor(
		private machineServices: MachineServices,
		private coinServices: CoinServices,
		private productServices: ProductServices
	) {}

	//Get Amount
	public getInsertedAmount(): number {
		return parseFloat(this.order.insertedAmount.toFixed(2));
	}

	//Get Product
	public getProduct(): ProductEntity {
		return this.order.product;
	}

	//Get Change
	public getChange(): number {
		return parseFloat(this.order.changeAmount.toFixed(2));
	}

	//Get Rejected Coins
	public getRejectedCoins(): number {
		return this.order.rejectedCoins;
	}

	//Make Change
	public askChange(product: number): void {
		if (this.order.insertedAmount > product) {
			this.order.changeAmount = this.getInsertedAmount() - product;
			this.machineServices.makeChange(this.order.changeAmount);
		}
	}

	//Reset Order
	public resetOrder(): void {
		this.order.insertedAmount = 0;
		this.order.changeAmount = 0;
		this.order.rejectedCoins = 0;
		this.order.product = {} as ProductEntity;
		this.machineServices.resetMachine();
	}

	//Action User : Insert Coin
	public insertCoin(
		size: CoinEntity['size'],
		weight: CoinEntity['weight']
	): void {
		try {
			//Find The Type of Coin that has been inserted
			this.coinServices.findCoinType(size, weight);
			const coinAmount = this.coinServices.getCoinType();

			//The Coin Is A valid coin
			if (coinAmount) {
				//Increment the amount of money of the order
				this.order.insertedAmount += coinAmount;
				//Update the display of the machine
				this.machineServices.updateMessage(
					`Amount inserted: ${this.getInsertedAmount()}`
				);
				//Check if the machine can do a change
				this.machineServices.checkExactChange();
			}

			//The coin is not valid
			else {
				this.order.rejectedCoins++;
			}
		} catch (error) {
			if (error instanceof Error) {
				this.machineServices.updateMessage(
					error.message ||
						'Something went wrong when inserting a coin'
				);
			}
		}
	}

	//Action User : Check Display
	public checkDisplay(): void {
		if (this.machineServices.getMachineState() === MachineState.soldOut) {
			//No money in the machine
			if (this.order.insertedAmount === 0) {
				this.machineServices.updateMessage('INSERT COIN');
			} else if (this.order.product) {
				this.machineServices.updateMessage(
					`REMAINING : ${this.getInsertedAmount()}`
				);
			}
		} else if (
			this.machineServices.getMachineState() ===
			MachineState.orderComplete
		) {
			this.order.insertedAmount = 0;
			this.machineServices.resetMachine();
		}
	}

	//Action User : Select a product
	public selectProduct(codeProduct: number): void {
		try {
			//Get the product
			const product = this.productServices.findProduct(codeProduct);

			if (product && product.name) {
				//Assign Product
				Object.assign(this.order.product, product);

				//Check if the product is available
				const isProductAvailable =
					this.machineServices.checkProductAvaibility(
						this.order.product
					);

				if (isProductAvailable === false) {
					this.machineServices.changeMachineState(
						MachineState.soldOut
					);
					// this.machineServices.updateMessage('SOLD OUT');
					throw new Error('SOLD OUT');
				}

				//Check if the user has the correct amount of money
				if (product.price > this.getInsertedAmount()) {
					const diff = product.price - this.getInsertedAmount();
					this.order.changeAmount = parseFloat(diff.toFixed(2));
					throw new Error(
						`PRICE : ${this.order.product.price}. INSERT ${this.order.changeAmount}`
					);
				}
				//Update the message on the machine
				this.machineServices.updateMessage('THANK YOU');
				//Make Change
				this.askChange(this.order.product.price);

				//Order Complete
				this.machineServices.changeMachineState(
					MachineState.orderComplete
				);

				setTimeout(() => {
					//Reset Order
					this.resetOrder();
				}, 500);
			}
		} catch (error) {
			if (error instanceof Error) {
				this.machineServices.updateMessage(
					error.message || 'Cannot Select Product'
				);
			}
		}
	}
}
