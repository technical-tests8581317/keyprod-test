import { ProductEntity } from '../product/product.Entity';

export interface OrderEntity {
	insertedAmount: number;
	changeAmount: number;
	rejectedCoins: number;
	product: ProductEntity;
}
