import { Container } from 'typedi';
import { OrderServices } from './order.Services';
import { CoinType } from '../../enums';
import { MachineServices } from '../machine/machine.Services';

describe('Order Testing Services - No Coins Inserted', () => {
	let orderServices: OrderServices;
	let machineServices: MachineServices;

	beforeAll(() => {
		orderServices = Container.get(OrderServices);
		machineServices = Container.get(MachineServices);
	});

	it('It Should show the message INSERT COIN on the machine.', () => {
		expect(machineServices.getMessage()).toBe('INSERT COIN');
	});
});

describe('Order Testing Services - Coins Inserted', () => {
	let orderServices: OrderServices;
	let machineServices: MachineServices;

	beforeAll(() => {
		orderServices = Container.get(OrderServices);
		machineServices = Container.get(MachineServices);
	});

	beforeEach(() => {
		//Insert a Dime - 0.1
		orderServices.insertCoin(17.91, 2.268);
		//Insert a Nickel - 0.05
		orderServices.insertCoin(21.21, 5.0);
		//Insert a Quarter - 0.25
		orderServices.insertCoin(24.257, 5.67);
	});

	afterEach(() => {
		orderServices.resetOrder();
	});

	describe('Service : Insert a coin', () => {
		it('It Should Increment the amount.', () => {
			expect(orderServices.getInsertedAmount()).toBe(0.4);
		});

		it('It Should show the amount inserted on the machine.', () => {
			expect(machineServices.getMessage()).toBe('Amount inserted: 0.4');
		});

		it('It should show the amount on the machine', () => {
			const message = machineServices.getMessage();
			expect(message).toMatch(
				`Amount inserted: ${orderServices.getInsertedAmount()}`
			);
		});

		it('Insert a wrong coin. It should increment the number of rejected coins', () => {
			orderServices.insertCoin(14.257, 2.67);
			orderServices.insertCoin(12.257, 1.67);
			orderServices.insertCoin(10.257, 3.67);
			const rejectedCoins = orderServices.getRejectedCoins();
			expect(rejectedCoins).toBe(3);
		});
	});

	describe('Service : Choose a product', () => {
		it('Choose a product that is not available', () => {
			orderServices.selectProduct(3);
			expect(machineServices.getMessage()).toBe('SOLD OUT');
		});

		it('Choose a product that is not available and display is checked again', () => {
			orderServices.selectProduct(3);
			orderServices.checkDisplay();
			expect(machineServices.getMessage()).toBe(
				`REMAINING : ${orderServices.getInsertedAmount()}`
			);
		});

		it('Choose a product that is available but is too expensive.', () => {
			orderServices.selectProduct(2);
			expect(machineServices.getMessage()).toBe(
				`PRICE : ${
					orderServices.getProduct().price
				}. INSERT ${orderServices.getChange()}`
			);
		});

		it('Choose a product that is available and affordable for the user.', () => {
			orderServices.selectProduct(1);
			expect(machineServices.getMessage()).toBe('THANK YOU');
		});

		it('Insert More Coins, Order and Make Change', () => {
			for (let i = 0; i < 3; i++) {
				orderServices.insertCoin(24.257, 5.67);
			}
			orderServices.selectProduct(1);
			expect(orderServices.getChange()).toBeGreaterThan(0);
		});

		it('The User Checks the display after choosing the product. The machine should show INSERT COIN', () => {
			orderServices.selectProduct(1);
			orderServices.checkDisplay();
			expect(machineServices.getMessage()).toBe('INSERT COIN');
		});

		it('The User Checks the display after choosing the product. The Inserted amount should be 0', () => {
			orderServices.selectProduct(1);
			orderServices.checkDisplay();
			expect(orderServices.getInsertedAmount()).toBe(0);
		});

		it('Choose a product that requires to make change.', () => {
			orderServices.insertCoin(10.257, 3.67);
			orderServices.selectProduct(1);
			expect(orderServices.getChange()).toBeGreaterThan(0);
		});

		it('The User Wants to return coins. The display should display the message INSERT COIN', () => {
			orderServices.resetOrder();
			expect(machineServices.getMessage()).toBe('INSERT COIN');
			expect(orderServices.getInsertedAmount()).toBe(0);
		});
	});
});
